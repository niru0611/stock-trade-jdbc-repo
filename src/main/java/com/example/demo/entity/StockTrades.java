package com.example.demo.entity;




public class StockTrades {
	


	private int id;
	
	private String stockTicker;
	
	private double price;
	
	private int volume;
	
	private String buyOrSell;
	
	private int statusCode;
	
	private String nameOfStock;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStockTicker() {
		return stockTicker;
	}

	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public String getBuyOrSell() {
		return buyOrSell;
	}

	public void setBuyOrSell(String buyOrSell) {
		this.buyOrSell = buyOrSell;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getNameOfStock() {
		return nameOfStock;
	}

	public void setNameOfStock(String nameOfStock) {
		this.nameOfStock = nameOfStock;
	}

	public StockTrades(int id, String stockTicker, double price, int volume, String buyOrSell, int statusCode,
			String nameOfStock) {
		super();
		this.id = id;
		this.stockTicker = stockTicker;
		this.price = price;
		this.volume = volume;
		this.buyOrSell = buyOrSell;
		this.statusCode = statusCode;
		this.nameOfStock = nameOfStock;
	}
	
	public StockTrades() {
		
	}
	
	


}
