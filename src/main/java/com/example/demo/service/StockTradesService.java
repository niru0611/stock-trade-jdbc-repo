package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.demo.entity.StockTrades;
import com.example.demo.repository.StockTradesRepository;


@Service
public class StockTradesService {
	
	@Autowired
	private StockTradesRepository repository;
	
	public List<StockTrades> getAllStockTrades() {
		return repository.getAllStocks();
	}
	
	public StockTrades getStockTrades(int id) {
		return repository.getById(id);
	}

	public StockTrades saveStockTrades(StockTrades trade) {
		return repository.editStockTrades(trade);
	}

	public StockTrades newStockTrades(StockTrades trade) {
		return repository.addStock(trade);
	}

	public int deleteStockTrades(int id) {
		return repository.deleteStock(id);
	}
	
}
