package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.StockTrades;

@Component
public interface StockTradesRepository {
	
	public List<StockTrades> getAllStocks();
	public StockTrades getById(int id); 
	public StockTrades editStockTrades(StockTrades trade);
	public int deleteStock(int id);
	public StockTrades addStock(StockTrades trade);

}
