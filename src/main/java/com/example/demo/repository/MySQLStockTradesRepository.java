package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.StockTrades;


@Repository
public class MySQLStockTradesRepository implements StockTradesRepository {
	
	@Autowired
	JdbcTemplate template;
	
	@Override
	public StockTrades getById(int id) {
	// TODO Auto-generated method stub
	String sql = "SELECT id, stockTicker, price, volume, buyOrSell, statusCode, nameOfStock FROM  StockTrades WHERE id=?";	
	return template.queryForObject(sql, new StockTradesRowMapper(), id);}
	
	public int deleteStock(int id) {
			
			String sql = "DELETE FROM StockTrades WHERE id = ?";
			template.update(sql,id);
			return id;
		}
	
	@Override
	public List<StockTrades> getAllStocks() {
		// TODO Auto-generated method stub
		String sql = "SELECT id, stockTicker, price, volume, buyOrSell, statusCode, nameOfStock FROM StockTrades";
		return template.query(sql, new StockTradesRowMapper());

	}
	
	
	@Override
	public StockTrades addStock(StockTrades trade) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO StockTrades(stockTicker, price, volume, buyOrSell, statusCode, nameOfStock)" + 
						"VALUES(?,?,?,?,?,?)";
		template.update(sql,trade.getStockTicker(),trade.getPrice(),trade.getVolume(), trade.getBuyOrSell(), trade.getStatusCode(),trade.getNameOfStock());
		return trade;
	}
	
	@Override
	public StockTrades editStockTrades(StockTrades trade) {
		// TODO Auto-generated method stub
		String sql = "UPDATE StockTrades SET stockTicker = ?, price = ?, volume = ?, buyOrSell = ?, statusCode = ?, nameOfStock = ? " +
				"WHERE id = ?";
		template.update(sql,trade.getStockTicker(),trade.getPrice(),trade.getVolume(), trade.getBuyOrSell(), trade.getStatusCode(),trade.getNameOfStock(),trade.getId());
		return trade;
	}







}



class StockTradesRowMapper implements RowMapper<StockTrades>{

	@Override
	public StockTrades mapRow(ResultSet rs, int rowNum) throws SQLException {

		return new StockTrades(rs.getInt("id"),
				rs.getString("stockTicker"),
				rs.getDouble("price"),rs.getInt("volume"), rs.getString("buyOrSell"), rs.getInt("statusCode"),rs.getString("nameOfStock"));
	}
	
}