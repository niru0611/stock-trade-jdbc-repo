package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.StockTrades;
import com.example.demo.service.StockTradesService;


@RestController
@RequestMapping("api/stocks")
public class StockTradesController {

	@Autowired
	StockTradesService service;
	
	@GetMapping(value = "/")
	public List<StockTrades> getAllStockTrades() {
		return service.getAllStockTrades();
	}
	
	@GetMapping(value = "/{id}")
	public StockTrades getStockTradesById(@PathVariable("id") int id) {
	  return service.getStockTrades(id);
	}

	@PostMapping(value = "/")
	public StockTrades addStockTrades(@RequestBody StockTrades trade) {
		return service.newStockTrades(trade);
	}

	@PutMapping(value = "/")
	public StockTrades editStockTrades(@RequestBody StockTrades trade) {
		return service.saveStockTrades(trade);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteStockTrades(@PathVariable("id") int id) {
		return service.deleteStockTrades(id);
	}
}
